import React, { useState } from "react";

import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./components/home/home";
import Index from "./components/index/index";
import Apps from "./components/app/app";
import Web from "./components/web/web";
import Casestudies from "./components/case-studies/case-studies";
import Contact from "./components/contact/contact";
export const Datatransfer = React.createContext();
function App() {
  const [change, setchange] = useState(false);
  const [back, setback] = useState(false);
  const [datas, setdatas] = useState("");// this one is back button action menus and web
  const [showResults, setShowResults] = useState(false);// this one footer showresults
  
  const [count, setcount] = useState(0);
  const a = setInterval(() => {//two components time set change component for using count
    setcount((Prev) => Prev + 1);
  }, 1000);
  if (count > 500) {
    clearInterval(a);
  }

  return (
    <Datatransfer.Provider
      value={{
        back: back,
        setback: setback,
        datas: datas,
        setdatas: setdatas,
        setShowResults: setShowResults,
        showResults: showResults,
        change: change,
        setchange: setchange,
      }}
    >
      <BrowserRouter>
        <Routes>
          <Route path="/" exact element={count < 500 ? <Home /> : <Index />} />
          <Route path="/index" element={<Index />} />

          <Route path="/app" element={<Apps />} />

          <Route path="/web" element={<Web />} />

          <Route path="/case-studies" element={<Casestudies />} />

          <Route path="/contact" element={<Contact />} />
        </Routes>
      </BrowserRouter>
    </Datatransfer.Provider>
  );
}
export default App;
