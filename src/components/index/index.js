import React from "react";
import Footer from "../../ui/footer/footer";
import Menus from "../../ui/menus/menus";
class Index extends React.Component {
  render() {
    return (
      <div className="flash-screen home">
        <div className="flash-container">
          <div className="flex-box welcome-section">
            <div className="wel">
              <div className="logo-section">
                <div className="logo-row tsi-logo-box no-animation">
                  <a href={"/"} className="tsi-logo no-animation">
                    <img src="assets/images/TSI-logo.png" alt="" />
                  </a>
                </div>

                <div className="logo-row ts-logo-text">
                  <a href="https://tendersoftware.com/">
                    <img src="assets/images/logo-text.png" alt="" />
                  </a>
                </div>
              </div>

              <div className="content-section">
                <p>
                  <a href={"/index"} className="targe-link">
                    Tender Software
                  </a>{" "}
                  provides highly skilled software development resources to U.S.
                  business IT departments, software companies, and design
                  studios.
                </p>

                <p>
                  We maintain top tier offshore development offices, to which
                  our clients can outsource software development, reducing their
                  costs while maintaining quality.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div>
          <Menus />
        </div>

        <div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default Index;
