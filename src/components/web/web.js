import React, { useContext } from "react";
import Footer from "../../ui/footer/footer";
import Menus from "../../ui/menus/menus";
import Topheader from "../../ui/tophead/topnav";
import webJson from "../../json/webjson";
import Slider from "react-slick";
import { Datatransfer } from "../../App";
export const Web = () => {
  const { setback } = useContext(Datatransfer);// setback means backbutton clicked action topheader
  const [showResultssimages, setShowResultssimages] = React.useState(false);//showResultss means images logo
  const [screenshot, setScreenShot] = React.useState("");//screenshot means slider images
  const handelclickimage = (imgsrc) => {
    setScreenShot(imgsrc);
    setShowResultssimages((screen) => !screen);
  };
  const handelClicklink = (datatraget) => {
    console.log(datatraget);
    window.open(datatraget, "_blank");
  };
  const settings = {
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: false,
    arrows: false,
    rows: 3,
  };
  return (
    <div className="flash-screen web app-page ">
      <div className="flash-container">
        <div className="flex-box">
          <Topheader alert="activated" />
          <div
            className={`content-section div-posstion-relative ${
              showResultssimages ? "" : ""
            }`}
          >
            <div className="website-listing">
              <div className="list-item">
                <Slider {...settings}>
                  {webJson.map((data, index) => {
                    return (
                      <div key={index}>
                        <div className="bcshadow">
                          <div className="website-logo">
                            <img src={data.img} alt="" />
                          </div>

                          <div className="btn-group view-btn">
                            <button
                              type="button"
                              dataimgsrc={data.dataimgsrc}
                              className={`btn btn-sm btn-primary dataimg ${
                                showResultssimages ? "img" : ""
                              }`}
                              
                              onClick={() => {
                                setback((current) => !current);
                                handelclickimage(data.dataimgsrc);
                              }}
                            >
                              {data.buttonpara}
                            </button>

                            <a>
                              <button
                                type="button"
                                datatraget={data.datatraget}
                                className="btn btn-sm btn-primary btndatahref"
                                // onClick={()=>setback(current=>!current)}
                                onClick={() => {
                                  handelClicklink(data.datatraget);
                                }}
                              >
                                {data.buttonlink}
                              </button>
                            </a>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </Slider>
                <div className={`overlaploader ${showResultssimages ? "img" : ""}`}>
                  <div>
                    <img
                      src="assets/images/Spinner-1s-200px.svg"
                      alt="loader"
                    />
                  </div>
                </div>
                <div
                  className={`overlapdiv  ${
                    showResultssimages ? "showfromleft" : ""
                  }`}
                >
                  {showResultssimages ? (
                    <img src={screenshot} alt="screenshot" />
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Menus />
      <Footer />
    </div>
  );
};

export default Web;
