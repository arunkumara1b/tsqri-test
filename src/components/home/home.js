import React from "react";
import styled, { keyframes } from "styled-components";
function blinkingEffect() {
  return keyframes`
    50%{
      opacity:0  
      ;}
    }
  `;
}
const AnimatedComponent = styled.div`
  animation: ${blinkingEffect} 1s linear infinite;
`;
const Home = () => {
  return (
    <div className="flash-screen index-page">
      <div className="flash-container">
        <div className="flex-box welcome-section">
          <div className="wel">
            <div className={`logo-section `}>
              <div className="header-bg">
                <img src="assets/images/top-bg.png" alt="" />
              </div>

              <div className="flashlogotext">
                <div className={`logo-row tsi-logo-box `}>
                  <AnimatedComponent>
                    <a href={"/index"} className="tsi-logo">
                      <span className="pinkBg">
                        <img src="assets/images/TSI-logo.png" alt="" />
                      </span>

                      <span className="pinkBg"></span>

                      <span className="pinkBg"></span>

                      <span className="pinkBg"></span>
                    </a>
                  </AnimatedComponent>
                </div>

                <div className="logo-row ts-logo-text">
                  <a href={"https://tendersoftware.com/"}>
                    <img src="assets/images/logo-text.png" alt="" />
                  </a>
                </div>
              </div>

              <div className="footer-bg">
                <div className="full-content-section">
                  <img src="assets/images/Veteran_owned-200.png" alt="" />
                </div>
                <img src="assets/images/bottom-bg.png" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
