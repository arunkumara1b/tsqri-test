import React from "react";
import Footer from "../../ui/footer/footer";
import Menus from "../../ui/menus/menus";
import Topheader from "../../ui/tophead/topnav";
function Studies() {

  return (
    <div className="flash-screen case-studies">
      <div className="flash-container">
        <div className="flex-box">
          <Topheader  alert="activated" />
          <div className="content-section case-study-content-area">
            <header className="entry-header">
              <h3 className="titlecolor">Case Study #1</h3>
            </header>
            <div className="entry-content">
              <h5 className="titlecolor">Project Scope:</h5>
              <p>
                This construction-industry client wanted to attract homebuilders
                to list building projects on their website, so that site
                visitors could review the builders&rsquo; features and product
                offerings.
              </p>
              <p>
                Based on this relatively simple requirement, Tender initially
                built a WordPress website, with some basic static content pages.
                When a change became necessary, the client would log in and
                update the site.
              </p>
              <p>
                As the client&rsquo;s business grew, they wanted to add more
                functionality to their website. WordPress is not optimal for a
                rapidly growing website, so Tender recommended the website be
                rebuilt with a new framework to improve the security and
                robustness of the system. After intensive discussions and
                reviews with the client, they opted to move forward with
                CodeIgniter Framework.
              </p>
              <p>
                A team of 5-6 Tender developers then commenced the project and
                completed it quickly, delivering a stylish and functional
                high-end website for the client.
              </p>
              <br />
              <h5 className="titlecolor">Product Features:</h5>
              <h6>
                <u>Search Engine</u>
              </h6>
              <p>
                The strongest feature of the website is the&nbsp;
                <em>Builder&rsquo;s Search Engine,</em>&nbsp;the backbone of the
                client&rsquo;s digital presence. The search algorithm driving
                the search engine was built by Tender, and allows site visitors
                to filter by criteria such as location, building type, price
                range, etc. The search engine then produces results according to
                the criteria selected, and prompts the user to contact the
                builder for more detail.
              </p>
              <p>
                The search results page has the ability to return listings in a
                photo gallery format, allowing visitors to save the photos and
                share with friends, as well as downloading floor plans. Visitors
                can also submit their reviews of each builder.
              </p>
              <br />
              <h6>
                <u>Homebuilder&rsquo;s Dashboard</u>
              </h6>
              <p>
                Tender added functionality for builders to register a vendor
                account within the website, giving them the ability to create
                and maintain their public profile, and to access a dashboard to
                manage the presentation of their profile, project listings,
                gallery photos, special offers, and more. We also developed a
                for-fee premium campaign feature which generated incremental
                revenue for the client.
              </p>
              <br />
              <h6>
                <u>Team Member User Profiles</u>
              </h6>
              <p>
                We created the ability for builder employees to create and log
                into their own subaccounts, permitting them to interact more
                personally and directly with potential customers on behalf of
                their employers.
              </p>
              <br />
              <h6>
                <u>Customer User Profiles</u>
              </h6>
              <p>
                We also built an option for end customers to register &ndash;
                using either their email address or Facebook account &ndash; an
                account profile, giving them the ability to forward project
                photos to their friends and contacts, and to submit photos to
                their personal &ldquo;idea board&rdquo;.
              </p>
              <br />
              <h5 className="titlecolor">Delivery and Impact:</h5>
              <p>
                In the early stages, this client was struggling to conceive and
                execute what they wanted to achieve. Tender&rsquo;s design
                suggestions provided the direction and confidence they needed to
                move forward with the implementation of a successful web
                presence. The client now has over 17,000 builders registered in
                their system, with an average of over 5,000 unique visitors for
                each builder. As of this writing, Tender is continuing to
                maintain the website and develop additional functionality for
                this client.
              </p>
              <br />
              <br />
              <h3 className="titlecolor">Case Study #2</h3>
              <h5 className="titlecolor">Project Scope:</h5>
              <p>
                A smaller client with a business model focused on web app
                integration provides their core functionality as SaaS. Their
                suite of applications enables their customers to access and
                manage multiple web portals including Gmail, Twitter, Facebook,
                and others from a single, consolidated interface. This avoids
                the need for their customers to log in to each portal
                individually, and streamlines the process of managing business
                messaging and user interaction across the web.
              </p>
              <p>
                Our initial task for this client was to develop a basic website
                and content, including a description of services and profile of
                the company. We implemented this website functionality via
                WordPress.
              </p>
              <p>
                Building on the success of that relatively simple project, the
                client asked us to help design and improve their core suite of
                web applications. To that end, we developed web extensions for
                unified management of contacts, email, invoices, and other key
                data. The extensions are available for customers to purchase as
                a package, and permit customers to integrate in one application
                suite the services from various major web portals. We developed
                these applications with a four-member team of developers using
                CodeIgniter and jQuery.
              </p>
              <br />
              <h5 className="titlecolor">Product Features:</h5>
              <p>
                In addition to integrating multiple web platforms, the
                application suite also maintains contacts (saving the contact
                details from all platforms) and permits a Bulk Email feature to
                send emails to the saved contacts across platforms.
              </p>
              <p>
                Some of the functionality built by Tender for this client
                includes:
              </p>
              <ul className="caststudy-ul">
                <li>
                  <strong>Gmail</strong>&nbsp;service within the platform
                  permits the user to create, reply, forward and view messages
                  in their Gmail account.
                </li>
                <li>
                  <strong>Google Drive</strong>&nbsp;integration allows files
                  from a specified Drive account to be created, shared, and
                  browsed from within the app. Customer and project files can be
                  maintained for users in the same place for Users, permitting
                  the easy linkage to user documents from within the
                  application.
                </li>
                <li>
                  <strong>Google Calendar&nbsp;</strong>Connects the
                  user&rsquo;s Google Calendar account and syncs all events and
                  tasks.
                </li>
                <li>
                  <strong>Xero</strong>&nbsp;integration enables the creation of
                  invoices for contacts maintained within the app. Users can
                  create, edit, send, and delete invoices, import and tag
                  existing customers, and use the application to data
                  automatically create new customers from the data.
                </li>
                <li>
                  <strong>Mailchimp</strong>&nbsp;functionality allows users to
                  sync their contacts and create bulk email distribution lists.
                </li>
                <li>
                  <strong>doTalk&nbsp;</strong>API allows users to initiate
                  calls to their contacts from within the application, and to
                  display call details including Call Duration, Call Type, and
                  Date of Call in their feed. Users can also record and play
                  back calls for training and quality assurance.
                </li>
                <li>
                  <strong>Facebook&nbsp;</strong>integration enables users to
                  post status updates and track statistics for each post, such
                  as Like Count, Share Count, etc. The application keeps track
                  of new likes and user engagement on business Facebook pages.
                  Users can promote their business by posting to and maintaining
                  their public Facebook profile without the overhead of logging
                  into the Facebook site or application itself.
                </li>
                <li>
                  <strong>Twitter&nbsp;</strong>functionality similarly permits
                  users to tweet from within the application, and will report
                  stats (such as ReTweet count) for each post.
                </li>
                <li>
                  <strong>WordPress&nbsp;</strong>plugin captures contact form
                  content and e-commerce order details to the client&rsquo;s
                  account via an authentication access token from client&rsquo;s
                  website to the application. Website inquiries automatically
                  create a new contact, importing their data and tagging them as
                  configured by the user.
                </li>
              </ul>
              <br />
              <h5 className="titlecolor">Delivery and Impact:</h5>
              <p>
                Initially, this project was small in scope and the user base was
                relatively minimal. With functionality across platforms not
                integrated, the products were scattershot in nature, leading to
                material wasted user effort. After our integration of the
                application suite and structured design to achieve a centralized
                architecture, user satisfaction improved significantly. The
                client has since been successful in selling the application
                package to a large user base which is growing by the day.
              </p>
              <br />
              <p>
                <strong>
                  <em>
                    Contact us today for additional case study information and
                    screenshot photos.
                  </em>
                </strong>
              </p>
              <br />
              <br />
              <br />
              <br />
            </div>
          </div>
        </div>
      </div>

      <div>
        <Menus />
      </div>

      <div>
        <Footer />
      </div>
    </div>
  );
}
export default Studies;
