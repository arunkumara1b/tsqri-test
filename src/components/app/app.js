import React from "react";
import Footer from "../../ui/footer/footer";
import Menus from "../../ui/menus/menus";
import Topheader from "../../ui/tophead/topnav";
import appJson from "../../json/appjson";
import Slider from "react-slick";
export const Apps = () => {
  const slider = {
    dots: true,
    infinite: true,
    centerMode: false,
    speed: 300,
    slidesToShow: 1,
    arrows: false,
    rows: 2,
  };
  return (
    <div className="flash-screen app app-page">
      <div className="flash-container">
        <div className="flex-box">
          <Topheader alert="notactivated" />
          <div className="content-section">
            <div className="website-listing">
              <div className="list-item">
              <Slider {...slider}>{/*slider using a moving action*/}
                  {appJson.map((data, index1) => {
                    return (
                      <div key={index1}>
                        <div className="bcshadow1">
                          <div className="website-logo">
                            <img src={data.img} alt="" />
                          </div>

                          <div className="btn-group view-btn1">
                            <a
                              href={data.link1}
                              className="btn btn-sm btn-primary"
                            >
                              <img src={data.imggoogle} alt="" />
                            </a>
                            <a
                              href={data.link2}
                              className="btn btn-sm btn-primary"
                            >
                              <img src={data.imgappple} alt="" />
                            </a>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </Slider>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Menus />
      <Footer />
    </div>

  );
};

export default Apps;
