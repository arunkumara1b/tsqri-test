import React, { useContext }  from "react";
// import { useState } from "react";
import { FaChevronLeft } from "react-icons/fa";
import { Datatransfer } from "../../App";
import Footer from "../../ui/footer/footer";
import Menus from "../../ui/menus/menus";

function Contact()  {
  

  const { setShowResults, back, datas } = useContext(Datatransfer);

  
  return (
    <div className="flash-screen contact app-page">
      <div className="flash-container">
        <div className="flex-box">
          <div className="app-header-section d-flex flex-column align-items-center px-sm-4">
            <div className="app-logo font-weight-normal">
              
              <span className="contact-back-button">
              {back?<a href={datas}  style={{border:"none",color:"#09a7f8"}}><i className="fa fa fa-chevron-left"><FaChevronLeft/></i></a>:<a onClick={()=>setShowResults(current=>!current)} style={{border:"none",color:"#09a7f8"}}><i className="fa fa fa-chevron-left"><FaChevronLeft/></i></a>} 
              </span>
              <a href={"https://tendersoftware.com"} className="contact-logo">
                <img src="assets/images/logo/contact-logo.svg" alt="" />
              </a>
            </div>
            <div className="app-title mr-auto ml-auto text-white">
              Request Info

            </div>
            
          </div>
          <div className="content-section">
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <form  
                    method="get"
                    data-form-title="CONTACT US"
                    id="contact_form"
                    action="/contact"
                  >
                    <input type="hidden" data-form-email="true" />

                    <div className="form-group"  >
                      <input
                        type="text" className="form-control"  required="" placeholder="NAME" data-form-field="Name"
                        
                        />
                    </div>

                    <div className="form-group">
                      <input
                        type="email"
                        className="form-control" required=""  placeholder="EMAIL"  data-form-field="Email"/>
                    </div>

                    <div className="form-group">
                      <input  type="email"  className="form-control" required=""  placeholder="CONFIRM EMAIL"  data-form-field="ConfirmEmail"
                      />
                    </div>

                    <div className="form-group">
                      <input type="tel" className="form-control"  placeholder="PHONE NUMBER" data-form-field="Phone"
                      />
                    </div>

                    <div className="form-group">
                      <input  type="tel"  className="form-control"  name="confirm_phone"  placeholder="CONFIRM PHONE NUMBER"  data-form-field="ConfirmPhone"
                      />
                    </div>

                    <div className="btn-group view-btn">
                      <button
                        type="submit"
                        className="btn btn-lg btn-primary sent-contact-btn"
                      >
                        SEND
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <Menus />
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default Contact;
