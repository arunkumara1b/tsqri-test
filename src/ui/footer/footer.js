import React, { useContext } from "react";
import Menus from "../menus/menus";
import { Datatransfer } from "../../App";
const Footer = () => {
  const { setShowResults, showResults } = useContext(Datatransfer);
  const onClick = () => {
    setShowResults((current) => !current);
  };
  return (
    <div className="flash-screen home">
      <div className={`menu-toggle ${showResults ? "open" : ""}`}>
        <div className="menu-icon d-flex">
          <div
            className={`menu-icons ${showResults ? "open" : ""}`}
            onClick={onClick}
          >
            <span></span>

            <span></span>

            <span></span>

            <span></span>
          </div>
        </div>
        {showResults ? <Menus menuopen={showResults ? "open" : ""} /> : null}
        <span className="menu-txt">{showResults ? "CLOSE" : "MENU"}</span>

        <span className="contact-txt">
          <a href={"/contact"}>CONTACT US</a>
        </span>
      </div>
    </div>
  );
};

export default Footer;
