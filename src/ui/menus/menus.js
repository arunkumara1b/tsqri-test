import React, { useContext } from "react";
import menusLink from "../../json/menujson";
import { useNavigate } from "react-router-dom";
import { Datatransfer } from "../../App";
export const Menus = ({ menuopen }) => {
  const { setdatas, setShowResults } = useContext(Datatransfer);
  const navigate = useNavigate();

  return (
    <div className={`menu-section ${menuopen}`}>
      <div className="flex-menu">
        <ul className="bottom-menu-items">
          {menusLink.map((data) => {
            return (
              <li key={data.label}>
                <a
                  onClick={() => {
                    setShowResults((current) => !current);
                    var a = data.link;
                    setdatas(a);
                    navigate(data.link);
                  }}
                  
                >
                  <div className="menu-contain">
                    <div className="menu-icon">
                      
                      <img src={data.img} alt="" />
                    </div>
                  </div>
                  <span className="txt" >{data.label}</span>
                </a>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};
export default Menus;
