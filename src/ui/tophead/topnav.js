import React, { useContext } from "react";
import { FaChevronLeft } from "react-icons/fa";
import { FaDownload } from "react-icons/fa";
import { Datatransfer } from "../../App";
import { useNavigate } from "react-router-dom";
const Topheader = (props) => {
  const navigate = useNavigate();
  const { setShowResults, back, datas } = useContext(Datatransfer);
  const handelclick = () => {
    // setchange(!change)
    // setBack(current => !current)
  };
  return (
    <div className="app-header-section d-flex flex-sm-row align-items-center px-sm-4">
      <div className="download-btn pagebacktomenubtn">
        <button
          className="d-flex align-items-center blue-txt text-uppercase cus-backbtn cus-backbtn-click"
          onClick={() => handelclick()}
        >
          {back ? (
            <a href={datas} style={{  }}>
              <i className="fa fa fa-chevron-left">
                <FaChevronLeft />
              </i>
            </a>
          ) : (
            <a
              onClick={() => setShowResults((current) => !current)}
              style={{  }}
            >
              <i className="fa fa fa-chevron-left">
                <FaChevronLeft />
              </i>
            </a>
          )}
        </button>
      </div>

      <div className="logo-box">
        <div className="app-logo font-weight-normal">
          <a href={"/index"} className="app-tsi-logo">
            <img src="assets/images/TSI-logo.png" alt="" />
          </a>
        </div>

        <div className="app-title mr-auto ml-2 blue-txt">WEBSITE PORTFOLIO</div>
      </div>

      <div className="download-btn ">
        <a
          download
          href="assets/pdf/TS-Mobile-App-Portfolio-2019.pdf"
          className="btn btn-outline-primary d-flex align-items-center blue-txt text-uppercase"
        >
          <i className="fa fa-download" aria-hidden="true">
            <FaDownload />
          </i>
        </a>
      </div>
    </div>
  );
};

export default Topheader;
