const menusLink= [   
    {
        "link":"/web",
        "img":"assets/images/web.svg",
        "label":"WEBSITE EXAMPLE"
    },
    {
        "link":"/app",
        "img":"assets/images/app.svg",
        "label":"MOBILE APP EXAMPLES"
    },
    {
        
        "link":"/case-studies",
        "img":"assets/images/case.svg",
        "label":"ENTERPRISE PROJECT EXAMPLES"
    },
    {
        "link":"/contact",
        "img":"assets/images/white-label.svg",
        "label":"CONTACT US"
    },
    {
        "link":"https://tendersoftware.com/white-label-services/",
        "img":"assets/images/contact.svg",
        "label":"WHITE LABEL SERVICES"
    },
    {
        "link":"https://tendersoftware.com/IoT/",
        "img":"assets/images/IOT.svg",
        "label":"IOT"
    },
    {
        "link":"https://tendersoftware.com/usa/docs/TenderSoft-DesignPortfolio.pdf",
        "img":"assets/images/graphic-design.svg",
        "label":"GRAPHIC DESIGN EXAMPLES"
    },
   
    {
        "link":"https://tendersoftware.com/usa/docs/TenderSoft-SEO.pdf",
        "img":"assets/images/SEO.svg",
        "label":"SEO"
    },
   
    
]

export default menusLink;