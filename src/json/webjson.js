
const webJson= [   
  
    {
     
        "img":"assets/images/logo/wotc-logo.png",
        "dataimgsrc":"assets/images/screenshot/wotc.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://wotcplanet.com/",
        "buttonlink":"View Website"

    },
    {
     
        "img":"assets/images/logo/safcol-logo.png",
        "dataimgsrc":"assets/images/screenshot/safcol.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://www.safcol.com.au/",
        "buttonlink":"Visit Website"

    },
    {
     
        "img":"assets/images/logo/typeset.png",
        "dataimgsrc":"assets/images/screenshot/typeset.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://typesetcontent.com/",
        "buttonlink":"View Website"

    },
    {
     
        "img":"assets/images/logo/innate-001.png",
        "dataimgsrc":"assets/images/screenshot/innate.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://innatebrewers.com.au/",
        "buttonlink":"Visit Website"

    },
    {
     
        "img":"assets/images/logo/marcem.png",
        "dataimgsrc":"assets/images/screenshot/marcem.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://wotcplanet.com/",
        "buttonlink":"View Website"

    },
    {
     
        "img":"assets/images/logo/avalon.png",
        "dataimgsrc":"assets/images/screenshot/avaloan.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://wotcplanet.com/",
        "buttonlink":"View Website"

    },
    {
     
        "img":"assets/images/logo/rockford.png",
        "dataimgsrc":"assets/images/screenshot/rockford.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://www.rockfordcareercollege.edu/",
        "buttonlink":"View Website"

    },
    {
     
        "img":"assets/images/logo/king-solar-logo.png",
        "dataimgsrc":"assets/images/screenshot/king-solar-logo.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://www.kingsolar.net/",
        "buttonlink":"View Website"

    },

    {
     
        "img":"assets/images/logo/sole-therapy-001.png",
        "dataimgsrc":"assets/images/screenshot/sole-therapy-001.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://soletherapy.com.au/",
        "buttonlink":"View Website"

    },
    {
     
        "img":"assets/images/logo/evp.png",
        "dataimgsrc":"assets/images/evp.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://www.evolutionplumbing.com.au",
        "buttonlink":"View Website"

    },
    {
     
        "img":"assets/images/logo/beansbeauty-logo.png",
        "dataimgsrc":"assets/images/beansbeauty-logo.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://www.beansbeauty.com",
        "buttonlink":"View Website"

    },
    {
     
        "img":"assets/images/logo/david.png",
        "dataimgsrc":"assets/images/david.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://davidshanahan.com.au",
        "buttonlink":"View Website"

    },
    {
     
        "img":"assets/images/logo/humble-grumble.png",
        "dataimgsrc":"assets/images/hg.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"http://humblegrumble.com.au/",
        "buttonlink":"View Website"
    },
    {
     
        "img":"assets/images/logo/coogee.png",
        "dataimgsrc":"assets/images/coogee.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://coogeelife.com.au/",
        "buttonlink":"View Website"
    },
    {
     
        "img":"assets/images/logo/fp.png",
        "dataimgsrc":"assets/images/fp.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://foundingpianos.com.au",
        "buttonlink":"View Website"
    },
    {
     
        "img":"assets/images/logo/ch.png",
        "dataimgsrc":"assets/images/ch.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://www.creativehanging.com.au/",
        "buttonlink":"View Website"
    },
    {
     
        "img":"assets/images/logo/sta.png",
        "dataimgsrc":"assets/images/sta.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://www.sctoday.edu/",
        "buttonlink":"View Website"
    },
    {
     
        "img":"assets/images/logo/wabar-logo.png",
        "dataimgsrc":"assets/images/WA.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://wabar.asn.au",
        "buttonlink":"View Website"
    },
    {
     
        "img":"assets/images/logo/wabar-logo.png",
        "dataimgsrc":"assets/images/WA.jpg",
        "buttonpara":"View Screenshot",
        "datatraget":"https://wabar.asn.au",
        "buttonlink":"View Website"
    },
  
]

export default webJson;