const appLink= [   
    {
        
        "img":"assets/images/logo/mangocam-logo.png",
        "link1":"https://play.google.com/store/apps/details?id=com.mangocam.viewer",
        "imggoogle":"assets/images/googleplay.png",
        "link2":"https://apps.apple.com/app/id646346609",
        "imgappple":"assets/images/appstore.png"
    },
    {
        
        "img":"assets/images/logo/switcharoo.png",
        "link1":"https://apps.apple.com/us/app/switcharooo/id1288952516",
        "imggoogle":"assets/images/googleplay.png",
        "link2":"https://apps.apple.com/app/id646346609",
        "imgappple":"assets/images/appstore.png"
    },
    {
        
        "img":"assets/images/logo/aftermath.png",
        "link1":"https://play.google.com/store/apps/details?id=com.pubsafe.app",
        "imggoogle":"assets/images/googleplay.png",
        "link2":"https://apps.apple.com/us/app/pubsafe-public-safety-network/id1448145838",
        "imgappple":"assets/images/appstore.png"
    },
    {
        
        "img":"assets/images/logo/goget.png",
        "link1":"https://itunes.apple.com/au/app/goget/id917053598?mt=8",
        "imggoogle":"assets/images/appstore.png",
        "link2":"assets/images/logo/scoreboard.png",
        "imgappple":"assets/images/appstore.png"
    },
   

]

export default appLink;